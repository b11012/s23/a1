/*
ACTIVITY: 

	>> Create a new set of pokemon for pokemon battle.

	>> Solve the health of the pokemon that when tackle is invoked, current value of target’s health should decrease continuously as many times the tackle is invoked
	(target health - this attack)
	
	>> If health is below 10, invoke faint function

*/

// create a set of pokemon for pokemon battle

function Pokemon (name, lvl){
	
	this.name = name;
	this.level = lvl;
	this.health = lvl * 2;
	this.attack = lvl; 
	this.tackle = function(opponent){
		
		if(opponent.health > 10){
		console.log(`${this.name} tackled ${opponent.name}`)
		
		opponent.health = opponent.health - this.attack
		console.log(`${opponent.name}'s health is now reduced to ${opponent.health}`)
		this.faint(opponent)

		} else if(opponent.health <= 10){
			console.log(`Stop attacking! ${opponent.name} has fainted`)
		}
		
		
	};
		this.faint = function(opponent){
			if (opponent.health <= 10){
			console.log(`${opponent.name} fainted`)
		}
	}
}

let jigglypuff = new Pokemon ("Jigglypuff", 30);

let snorlax = new Pokemon ("Snorlax", 30);

jigglypuff.tackle(snorlax);
snorlax.tackle(jigglypuff);
jigglypuff.tackle(snorlax);
snorlax.tackle(jigglypuff);
jigglypuff.tackle(snorlax);
jigglypuff.tackle(snorlax);

